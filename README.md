# CPP Project CookieCutter Template

![Build status](https://git.astron.nl/templates/cpp-project/badges/main/pipeline.svg)
![Test coverage](https://git.astron.nl/templates/cpp-project/badges/main/coverage.svg)

An example repository of an CI/CD pipeline for building, testing and publishing a python package.

If you find some missing functionality with regards to CI/CD, testing, linting or something else, feel free to make a merge request with the proposed changes.

## How to apply this template

This templates uses `cookiecutter` which can be easily installed:

```bash
pip install --user cookiecutter
```

Then you can create an instance of your template with:

```bash
cookiecutter https://git.astron.nl/templates/cpp-project.git
# Next follow a set of prompts (such as the name and description of the package)
```

## License
This project is licensed under the Apache License Version 2.0
