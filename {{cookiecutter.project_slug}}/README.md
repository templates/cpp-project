# {{cookiecutter.project_name}}

{{cookiecutter.description}}

## Description

The repository can be used as a basis for new projects using C++ within ASTRON.
The code is intended to be copied in a new repository and used as basis for the
new project. After copying the code needs to be adjusted for the specific
project. The template offers:

- CI jobs for building, testing, and generating documentation

  - A basic Docker file to be used in the CI
  - A CMake project skeleton
  - A Doxygen and Sphinx skeleton for documentation, combined with Breathe
  - Formatting scripts for the code

Note: The project is intended to be used for C++ projects without Fortran code.
At the moment of writing there is no Fortran support for Ninja. This means the
project needs to be changed to use `make`.

## Getting started

- Install all dependencies as listed in `docker/ci-runnner/Dockerfile`
- Create build directory: `mkdir build`
- Create the project: `cmake -DBUILD_TESTING=ON -DBUILD_DOCUMENTATION=ON -B ./build -G Ninja`
- Build the project: `ninja -C ./build`

Specific targets can also be build on a case per case basis use `ninja -C ./build/ -t targets`
to list all of them.

For instance:
- Build the documentation: `ninja -C ./build/ sphinx`
- Run unit tests: `ninja -C ./build/ test`
- Install the project globally: ``ninja -C ./build/ install``
- Clean caches: `ninja -C ./build/ clean`

## Versioning

This project uses git tags or branch names to determine the current version of the
software. The version information is automatically made available in CMake and can be
made available in C/C++ by dynamically adding a generated header.

See for details:
- https://github.com/jahnf/CMake-GitVersion/blob/develop/cmake/GitVersion.cmake

## Contributing

To contribute, please create a feature branch and a "Draft" merge request. Upon
completion, the merge request should be marked as ready and a reviewer should be
assigned.

## License
This project is licensed under the Apache License Version 2.0.
