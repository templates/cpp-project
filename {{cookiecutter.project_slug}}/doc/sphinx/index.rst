…

# Documentation
# https://breathe.readthedocs.io/en/latest/directives.html

Docs
====

This text is added through ReStructuredText (RST) in Sphinx while the below
is derived from Doxygen:

.. doxygenindex::
