// Copyright (C) ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef CPP_INCLUDE_HELLO_HPP
#define CPP_INCLUDE_HELLO_HPP

namespace lib {

/** This function greets the user.
 *
 * @code
 * char *buffer = new char[42];
 * int charsAdded = sprintf(buffer, "Include code\n", 8);
 * @endcode
 */
void Hello();

} // namespace lib

#endif // CPP_INCLUDE_HELLO_HPP
