# Copyright (C) ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

add_library(hello SHARED hello.cpp)

target_include_directories(hello PUBLIC "${CMAKE_SOURCE_DIR}/include")

install(TARGETS hello)
