// Copyright (C) ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include "hello.hpp"

#include <iostream>
#include <string>

namespace lib {

void Hello() { std::cout << "Hello world\n"; }

} // namespace lib
