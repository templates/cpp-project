// Copyright (C) ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <iostream>

#include "hello.hpp"
#include "cpp-GitVersion.h"

int main(int, const char **) {
    lib::Hello();

    std::cout << "CMake-GitVersion example"<< std::endl;
    std::cout << "- version: " << cpp::version() << std::endl;
    std::cout << "- version_string: " << cpp::version_string() << std::endl;
    std::cout << "- version_branch: " << cpp::version_branch() << std::endl;
    std::cout << "- version_fullhash: " << cpp::version_fullhash() << std::endl;
    std::cout << "- version_shorthash: " << cpp::version_shorthash() << std::endl;
    std::cout << "- version_isdirty: " << cpp::version_isdirty() << std::endl;
    std::cout << "- version_distance: " << cpp::version_distance() << std::endl;
    std::cout << "- version_flag: " << cpp::version_flag() << std::endl;
}
