// Copyright (C) ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include "hello.hpp"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(hello)

BOOST_AUTO_TEST_CASE(hello) { lib::Hello(); }

BOOST_AUTO_TEST_SUITE_END()
